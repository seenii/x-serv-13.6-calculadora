"""
Crea un programa que sirva de calculadora y que incluya dos funciones (sumar,
restar), que han de llamarse para sumar 1 y 2, 3 y 4, y para restar 5 de 6 y 7 de 8.
"""

def sumar(num1:int, num2:int) -> int:
    return num1+num2

def restar(num1:int, num2:int) -> int:
    return num1-num2

print(sumar(1,2))
print(sumar(3,4))
print(restar(5,6))
print(restar(7,8))
